### rightround
Golang-lib for using a SPICE-format ephemeris: [EPM](http://iaaras.ru/dept/ephemeris/epm/) & [DE](https://ssd.jpl.nasa.gov/?ephemerides).

#### Example
```golang
const (
    pathEPM = "ephemeris/epm2017h.bsp"
    pathDE = "ephemeris/de441.bsp"
)

// loading ephemeris 'EPM2017'
ephemeris := rightround.NewEphemeris()
if err := ephemeris.LoadFile(pathEPM); err != nil {
    return err
}

julianDays, julianTime := 2453521.0, 0.25

// computing Mercury's position relative to the Earth
// at 00:00:00 30 May 2005
coords, _, err := ephemeris.CalculateRectangularCoordsAndScaleVelocity(gorewind.EphemerisMercury, gorewind.EphemerisEarth, julianDays, julianTime, false)
if err != nil {
    return err
}

// result in kilometers
fmt.Printf("%.5f %.5f %.5f\n", coords.X, coords.Y, coords.Z)
// output: -151786440.78263 -28597178.81489 -18024058.24283
```
---
#### Links
* [iaaras/ephemeris-access/libephaccess](https://gitlab.iaaras.ru/iaaras/ephemeris-access)